import pygame

from constants import *
from lander import Lander


def draw_text(win, text, pos, color=WHITE):
    font = pygame.font.Font(None, 36)
    text_surf = font.render(text, True, color)
    text_rect = text_surf.get_rect(topleft=pos)
    win.blit(text_surf, text_rect)


def draw_info(win, fps, fuel, vx, vy, angle):
    draw_text(win, f"FPS: {fps}", (10, 10))
    draw_text(win, f"Fuel: {fuel}", (10, 50))
    draw_text(win, f"Vx: {vx:.2f}", (10, 90))
    draw_text(win, f"Vy: {vy:.2f}", (10, 130))
    draw_text(win, f"Angle: {angle}º", (10, 170))


def draw_button(win, text, pos, size):
    font = pygame.font.Font(None, 36)
    button_rect = pygame.Rect(pos[0], pos[1], size[0], size[1])
    pygame.draw.rect(win, WHITE, button_rect)
    text_surf = font.render(text, True, BLACK)
    text_rect = text_surf.get_rect(center=button_rect.center)
    win.blit(text_surf, text_rect)
    return button_rect


def initialize_landers(num_agents, lander_image, fire_image, thrust_sound, moon_mask, ground_y):
    landers = []

    for _ in range(num_agents):
        lander = Lander(WIDTH // 2, HEIGHT // 4, lander_image, fire_image, thrust_sound, moon_mask, ground_y)
        landers.append(lander)

    return landers


def reset_landers(landers):
    for lander in landers:
        lander.__init__(WIDTH // 2, HEIGHT // 4, lander.image_original, lander.fire_image, lander.thrust_sound,
                        lander.moon_mask, lander.ground_y)
