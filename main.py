import numpy as np
import pygame
import time

from ai import calculate_reward, DQNAgent, train_dqn_agents, get_state
from constants import *
from assets import load_assets
from utils import draw_info, initialize_landers, reset_landers, draw_button
from lander import Lander

# Инициализация Pygame
pygame.init()
pygame.mixer.init()


def manual_mode(WIN, background_image, moon_image, moon_mask_offset, lander_image, fire_image, thrust_sound, moon_mask,
                ground_y):
    landers = initialize_landers(1, lander_image, fire_image, thrust_sound, moon_mask, ground_y)
    lander = landers[0]
    run = True
    game_over = False
    clock = pygame.time.Clock()

    while run:
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            lander.rotate(-1)
        if keys[pygame.K_RIGHT]:
            lander.rotate(1)
        if keys[pygame.K_UP]:
            lander.thrust()

        lander.update()

        WIN.blit(background_image, (0, 0))
        WIN.blit(moon_image, moon_mask_offset)
        lander.draw(WIN)

        if lander.crashed or lander.landed:
            game_over = True

        if game_over:
            lander.draw_result(WIN)
            restart_button = draw_button(WIN, "Restart", (WIDTH // 2 - 50, HEIGHT // 2 + 50), (100, 50))
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    run = False
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if restart_button and restart_button.collidepoint(event.pos):
                        lander = Lander(WIDTH // 2, HEIGHT // 4, lander_image, fire_image, thrust_sound, moon_mask,
                                        ground_y)
                        game_over = False
                if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                    lander = Lander(WIDTH // 2, HEIGHT // 4, lander_image, fire_image, thrust_sound, moon_mask,
                                    ground_y)
                    game_over = False
        else:
            draw_info(WIN, round(clock.get_fps()), lander.fuel, lander.vx, lander.vy, lander.angle)

        pygame.display.update()
        clock.tick(TARGET_FPS)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                lander = Lander(WIDTH // 2, HEIGHT // 4, lander_image, fire_image, thrust_sound, moon_mask, ground_y)
                game_over = False

    pygame.quit()


def ai_mode(WIN, background_image, moon_image, moon_mask_offset, lander_image, fire_image, thrust_sound, moon_mask,
            ground_y):
    num_agents = 10  # Количество агентов
    landers = initialize_landers(num_agents, lander_image, fire_image, thrust_sound, moon_mask, ground_y)
    state_size = 5  # vx, vy, x, y, fuel
    action_size = 3  # rotate left, rotate right, thrust
    agents = [DQNAgent(state_size, action_size) for _ in range(num_agents)]

    while True:
        best_agent, best_lander = train_dqn_agents(WIN, landers, agents, background_image, moon_image, moon_mask_offset)
        # Демонстрация лучшего агента
        demonstrate_agent(WIN, best_agent, best_lander, background_image, moon_image, moon_mask_offset)
        time.sleep(2)  # Пауза на 2 секунды перед началом нового цикла


def demonstrate_agent(WIN, agent, lander, background_image, moon_image, moon_mask_offset):
    from utils import draw_info  # Импортируем внутри функции, чтобы избежать циклического импорта
    clock = pygame.time.Clock()
    state = np.array(get_state(lander))
    run = True

    while run:
        WIN.blit(background_image, (0, 0))
        WIN.blit(moon_image, moon_mask_offset)

        action = agent.act(state)
        if action == 0:
            lander.rotate(-1)
        elif action == 1:
            lander.rotate(1)
        elif action == 2:
            lander.thrust()
        lander.update()
        next_state = np.array(get_state(lander))
        state = next_state

        lander.draw(WIN)
        draw_info(WIN, round(clock.get_fps()), lander.fuel, lander.vx, lander.vy, lander.angle)
        pygame.display.update()
        clock.tick(TARGET_FPS)

        if lander.crashed or lander.landed:
            run = False

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                run = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                run = False

    lander.draw_result(WIN)
    pygame.display.update()
    pygame.time.wait(2000)  # Пауза на 2 секунды для отображения результата


def main():
    WIN = pygame.display.set_mode((WIDTH, HEIGHT))
    pygame.display.set_caption("Lunar Lander")

    # Загрузка изображений и звуков
    background_image, moon_image, lander_image, fire_image, thrust_sound = load_assets()

    # Установка иконки окна с использованием lander_image
    pygame.display.set_icon(lander_image)

    # Создание маски для поверхности луны
    moon_mask = pygame.mask.from_surface(moon_image)
    ground_y = HEIGHT - moon_image.get_height()
    moon_mask_offset = (0, ground_y)

    if USE_AI:
        ai_mode(WIN, background_image, moon_image, moon_mask_offset, lander_image, fire_image, thrust_sound, moon_mask,
                ground_y)
    else:
        manual_mode(WIN, background_image, moon_image, moon_mask_offset, lander_image, fire_image, thrust_sound,
                    moon_mask, ground_y)


if __name__ == "__main__":
    main()
