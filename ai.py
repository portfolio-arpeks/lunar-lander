import math
import random
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
from collections import deque
from constants import *
import pygame


class DQN(nn.Module):
    def __init__(self, state_size, action_size):
        super(DQN, self).__init__()
        self.fc1 = nn.Linear(state_size, 128)
        self.fc2 = nn.Linear(128, 128)
        self.fc3 = nn.Linear(128, action_size)

    def forward(self, x):
        x = torch.relu(self.fc1(x))
        x = torch.relu(self.fc2(x))
        return self.fc3(x)


class DQNAgent:
    def __init__(self, state_size, action_size, gamma=0.99, epsilon=1.0, epsilon_min=0.01, epsilon_decay=0.995,
                 lr=0.001, batch_size=64, memory_size=10000):
        self.state_size = state_size
        self.action_size = action_size
        self.gamma = gamma
        self.epsilon = epsilon
        self.epsilon_min = epsilon_min
        self.epsilon_decay = epsilon_decay
        self.batch_size = batch_size
        self.memory = deque(maxlen=memory_size)
        self.model = DQN(state_size, action_size)
        self.target_model = DQN(state_size, action_size)
        self.update_target_model()
        self.optimizer = optim.Adam(self.model.parameters(), lr=lr)
        self.criterion = nn.MSELoss()

    def update_target_model(self):
        self.target_model.load_state_dict(self.model.state_dict())

    def remember(self, state, action, reward, next_state, done):
        self.memory.append((state, action, reward, next_state, done))

    def act(self, state):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
        state = torch.FloatTensor(state).unsqueeze(0)
        with torch.no_grad():
            action_values = self.model(state)
        return np.argmax(action_values.cpu().data.numpy())

    def replay(self):
        if len(self.memory) < self.batch_size:
            return
        minibatch = random.sample(self.memory, self.batch_size)
        states, actions, rewards, next_states, dones = zip(*minibatch)

        states = np.array(states)
        next_states = np.array(next_states)

        states = torch.FloatTensor(states)
        actions = torch.LongTensor(actions)
        rewards = torch.FloatTensor(rewards)
        next_states = torch.FloatTensor(next_states)
        dones = torch.FloatTensor(dones)

        q_values = self.model(states).gather(1, actions.unsqueeze(1)).squeeze(1)
        next_q_values = self.target_model(next_states).max(1)[0]
        expected_q_values = rewards + (self.gamma * next_q_values * (1 - dones))

        loss = self.criterion(q_values, expected_q_values.detach())
        self.optimizer.zero_grad()
        loss.backward()
        self.optimizer.step()

        if self.epsilon > self.epsilon_min:
            self.epsilon *= self.epsilon_decay

    def reset_epsilon(self):
        self.epsilon = 1.0

    def inherit_weights(self, best_agent):
        self.model.load_state_dict(best_agent.model.state_dict())


def get_state(lander):
    vx = max(0, min(int((lander.vx + 5) // 1), 9))  # Ограничиваем диапазон от 0 до 9
    vy = max(0, min(int((lander.vy + 5) // 1), 9))  # Ограничиваем диапазон от 0 до 9
    x = max(0, min(int(lander.x // 20), 39))  # Ограничиваем диапазон от 0 до 39
    y = max(0, min(int(lander.y // 20), 29))  # Ограничиваем диапазон от 0 до 29
    fuel = max(0, min(int(lander.fuel // 10), 9))  # Ограничиваем диапазон от 0 до 9
    return [vx, vy, x, y, fuel]


def calculate_reward(lander):
    angle_in_radians = math.radians(abs(lander.angle))
    angle_reward = -angle_in_radians * ANGLE_COEF
    fuel_reward = lander.fuel * FUEL_COEF
    vy_reward = -abs(lander.vy) * VELOCITY_COEF if abs(lander.vy) > SAFE_LANDING_SPEED else (SAFE_LANDING_SPEED - abs(
        lander.vy)) * VELOCITY_COEF
    vx_reward = -abs(lander.vx) * HORIZONTAL_VELOCITY_COEF
    result_reward = CRASH_PENALTY if lander.crashed else LANDING_REWARD if lander.landed else 0
    total_reward = angle_reward + fuel_reward + vy_reward + vx_reward + result_reward

    if abs(lander.angle) > 10:  # Угол ограничения 10 градусов
        total_reward -= 50

    return total_reward


def train_dqn_agents(win, landers, agents, background_image, moon_image, moon_mask_offset):
    from utils import draw_info  # Импортируем внутри функции, чтобы избежать циклического импорта
    clock = pygame.time.Clock()
    gen = 0
    running = True
    max_reward_all = None

    while running:
        gen += 1
        total_rewards = [0] * len(agents)
        for lander in landers:
            lander.__init__(WIDTH // 2, HEIGHT // 4, lander.image_original, lander.fire_image, lander.thrust_sound,
                            lander.moon_mask, lander.ground_y)

        while any(not lander.crashed and not lander.landed for lander in landers):
            for i, (lander, agent) in enumerate(zip(landers, agents)):
                if not lander.crashed and not lander.landed:
                    state = np.array(get_state(lander))
                    action = agent.act(state)
                    if action == 0:
                        lander.rotate(-1)
                    elif action == 1:
                        lander.rotate(1)
                    elif action == 2:
                        lander.thrust()
                    lander.update()
                    next_state = np.array(get_state(lander))
                    reward = calculate_reward(lander)
                    total_rewards[i] += reward
                    done = lander.crashed or lander.landed
                    agent.remember(state, action, reward, next_state, done)
                    state = next_state

            # Обновление экрана и отображение информации на каждом шагу
            win.fill(BLACK)
            win.blit(background_image, (0, 0))
            win.blit(moon_image, moon_mask_offset)
            for lander in landers:
                lander.draw(win)
            fps = clock.get_fps()
            max_reward = max(total_rewards)
            if max_reward_all is None or max_reward > max_reward_all:
                max_reward_all = max_reward
            draw_info(win, round(fps), landers[0].fuel, landers[0].vx, landers[0].vy, landers[0].angle)
            pygame.display.set_caption(
                f"Lunar Lander | Generation: {gen} | Best Reward: {round(max_reward_all)} | Best Reward (current): {round(max_reward)}")
            pygame.display.update()
            clock.tick(TARGET_FPS)

        for agent in agents:
            agent.replay()
        for agent in agents:
            agent.update_target_model()

        best_agent_index = np.argmax(total_rewards)
        best_agent = agents[best_agent_index]

        for agent in agents:
            if agent != best_agent:
                agent.inherit_weights(best_agent)

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
                running = False
                return best_agent, landers[best_agent_index]  # Возвращаем лучшего агента и его ландер

    for agent in agents:
        agent.reset_epsilon()

    return None, None
