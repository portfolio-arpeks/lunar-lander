# Размеры окна
WIDTH, HEIGHT = 800, 600

# Цвета
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)

# Константы для лунного модуля
GRAVITY = 0.08
THRUST = -0.1
ROTATION_SPEED = 3
SAFE_LANDING_SPEED = 1

# Коэффициенты для награды агента
ANGLE_COEF = 2.0
FUEL_COEF = 0.5
VELOCITY_COEF = 5.0
HORIZONTAL_VELOCITY_COEF = 5.0
CRASH_PENALTY = -100
LANDING_REWARD = 200

TARGET_FPS = 144  # Целевой FPS
ASSETS_PATH = "assets"  # Путь к асетам
USE_AI = 1  # Установите True для использования ИИ или False для ручного управления
NUM_AGENTS = 10  # Количество агентов в режиме ИИ
STATE_SIZE = 10 * 10 * 40 * 30 * 10  # Размер пространства состояний
