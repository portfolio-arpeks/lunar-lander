import pygame
from constants import ASSETS_PATH


def load_image(path, size):
    image = pygame.image.load(path)
    return pygame.transform.scale(image, size)


def load_sound(path):
    return pygame.mixer.Sound(path)


def load_assets():
    background_image = load_image(ASSETS_PATH + "/images/space.png", (1920, 1080))
    moon_image = load_image(ASSETS_PATH + "/images/moon.png", (1200, 80))
    lander_image = load_image(ASSETS_PATH + "/images/lander.png", (64, 58))
    fire_image = load_image(ASSETS_PATH + "/images/fire.png", (30, 45))

    thrust_sound = load_sound(ASSETS_PATH + "/sounds/thrust.wav")

    return background_image, moon_image, lander_image, fire_image, thrust_sound
