import pygame
import math
from constants import *


class Lander:
    def __init__(self, x, y, lander_image, fire_image, thrust_sound, moon_mask, ground_y):
        self.x = x
        self.y = y
        self.angle = 0
        self.vx = 0
        self.vy = 0
        self.fuel = 100
        self.crashed = False
        self.landed = False
        self.image_original = lander_image
        self.image = lander_image
        self.rect = self.image.get_rect(center=(x, y))
        self.fire_image_original = fire_image
        self.fire_image = fire_image
        self.thrusting = False
        self.thrust_sound = thrust_sound
        self.moon_mask = moon_mask
        self.ground_y = ground_y

        # Create mask for lander
        self.mask = pygame.mask.from_surface(self.image)

    def rotate(self, direction):
        if not self.crashed and not self.landed:
            self.angle += direction * ROTATION_SPEED
            self.image = pygame.transform.rotate(self.image_original, -self.angle)
            self.rect = self.image.get_rect(center=self.rect.center)
            self.mask = pygame.mask.from_surface(self.image)  # Update mask after rotation

    def thrust(self):
        if self.fuel > 0 and not self.crashed and not self.landed:
            rad = math.radians(self.angle)
            self.vx -= THRUST * math.sin(rad)  # Correcting the direction of thrust
            self.vy += THRUST * math.cos(rad)
            self.fuel -= 1
            self.thrusting = True
            if not pygame.mixer.get_busy():
                self.thrust_sound.play()
        else:
            self.thrusting = False

    def update(self):
        if not self.crashed and not self.landed:
            self.vy += GRAVITY
            self.x += self.vx
            self.y += self.vy
            self.rect.center = (self.x, self.y)
            self.mask = pygame.mask.from_surface(self.image)  # Update mask after movement

            # Check for boundary conditions
            if self.rect.left < 0 or self.rect.right > WIDTH or self.rect.top < 0 or self.rect.bottom > HEIGHT:
                self.crashed = True
            else:
                self.check_collision()

        if self.crashed or self.landed:
            self.thrust_sound.stop()
        if not self.thrusting:
            self.thrust_sound.stop()

    def check_collision(self):
        # Check collision using masks
        offset = (self.rect.left, self.rect.top - self.ground_y)
        collision_point = self.moon_mask.overlap(self.mask, offset)
        if collision_point:
            if abs(self.vy) > SAFE_LANDING_SPEED or abs(self.angle) > 10:
                self.crashed = True
            else:
                self.landed = True

    def draw(self, win):
        win.blit(self.image, self.rect.topleft)

        if self.thrusting and not self.crashed and not self.landed:
            fire_image_rotated = pygame.transform.rotate(self.fire_image_original, -self.angle)
            fire_offset_y = self.rect.height // 2
            fire_x = self.rect.centerx - fire_offset_y * math.sin(math.radians(self.angle))
            fire_y = self.rect.centery + fire_offset_y * math.cos(math.radians(self.angle))
            fire_rect = fire_image_rotated.get_rect(center=(fire_x, fire_y))
            win.blit(fire_image_rotated, fire_rect.topleft)

        self.thrusting = False  # Reset thrusting status after drawing

    def draw_result(self, win):
        font = pygame.font.Font(None, 48)
        win.blit(self.image, self.rect.topleft)  # Draw the lander at the last position
        if self.crashed:
            text = font.render("Crashed!", True, RED)
        elif self.landed:
            text = font.render("Landed Successfully!", True, GREEN)
        else:
            return
        text_rect = text.get_rect(center=(WIDTH // 2, HEIGHT // 2))
        win.blit(text, text_rect)
